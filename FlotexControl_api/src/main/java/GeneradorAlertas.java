import controladores.AlertaControlador;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class GeneradorAlertas {

    public static void main(String[] args) {
        AlertaControlador controlador = new AlertaControlador();

        String[] tipos = {"Mantenimiento", "Reparaciones", "Documentación", "Seguridad", "Inspección", "Limpieza", "Combustible", "Carga"};
        String[] descripciones = {
                "Mantenimiento preventivo requerido para el vehículo #",
                "Reparación de frenos necesaria en el vehículo #",
                "Renovación de documentación vehicular pendiente para el vehículo #",
                "Revisión de equipos de seguridad necesaria en el vehículo #",
                "Inspección técnica vehicular programada para el vehículo #",
                "Servicio de limpieza interior y exterior pendiente para el vehículo #",
                "Nivel de combustible bajo en el vehículo #",
                "Sobrepeso en la carga detectado en el vehículo #"
        };

        // Generación de fechas aleatorias entre diciembre de 2023 y la fecha actual
        Calendar fechaInicio = new GregorianCalendar(2023, Calendar.DECEMBER, 1);
        Calendar fechaFin = Calendar.getInstance();
        long inicio = fechaInicio.getTimeInMillis();
        long fin = fechaFin.getTimeInMillis();

        for (int i = 0; i < 15; i++) {
            long fechaAleatoria = inicio + (long) (Math.random() * (fin - inicio));
            Calendar fecha = Calendar.getInstance();
            fecha.setTimeInMillis(fechaAleatoria);

            int tipoIndex = i % tipos.length;
            String tipo = tipos[tipoIndex];

            String descripcion = descripciones[tipoIndex].replace("#", Integer.toString(i + 1));

            int ID_usuario = 1 + (int)(Math.random() * ((5 - 1) + 1));

            controlador.crearAlerta(tipo, fecha.getTime(), descripcion, ID_usuario);
        }
    }
}

