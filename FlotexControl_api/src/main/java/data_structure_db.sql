-- Crear la base de datos
CREATE DATABASE IF NOT EXISTS flotexcontrol;

-- Usar la base de datos
USE flotexcontrol;

-- Tabla para almacenar los usuarios
CREATE TABLE IF NOT EXISTS usuarios
(
    ID_usuario  INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(100) NOT NULL,
    apellidos    VARCHAR(250) NOT NULL,
    username    VARCHAR(100) NOT NULL,
    contrasenia VARCHAR(100) NOT NULL,
    rol         INT          NOT NULL
);

-- Tabla para almacenar los vehículos
CREATE TABLE IF NOT EXISTS vehiculos
(
    ID_vehiculo     INT AUTO_INCREMENT PRIMARY KEY,
    matricula       VARCHAR(10)    NOT NULL,
    marca           VARCHAR(100)   NOT NULL,
    modelo          VARCHAR(100)   NOT NULL,
    anio            INT            NOT NULL,
    tipoCombustible VARCHAR(50)    NOT NULL,
    capacidadCarga  DECIMAL(10, 2) NOT NULL,
    tipoMotor       VARCHAR(50)    NOT NULL
);
-- Tabla para almacenar los conductores
CREATE TABLE IF NOT EXISTS conductores
(
    ID_conductor      INT AUTO_INCREMENT PRIMARY KEY,
    nombre            VARCHAR(100) NOT NULL,
    apellido          VARCHAR(100) NOT NULL,
    licencia_conducir VARCHAR(20)  NOT NULL,
    ID_vehiculo       INT,
    FOREIGN KEY (ID_vehiculo) REFERENCES vehiculos (ID_vehiculo)
);

-- Tabla para almacenar los roles de los usuarios
CREATE TABLE IF NOT EXISTS roles
(
    ID_rol    INT AUTO_INCREMENT PRIMARY KEY,
    nombreRol VARCHAR(100) NOT NULL
);


-- Tabla para almacenar los mantenimientos
CREATE TABLE IF NOT EXISTS mantenimientos
(
    ID_mantenimiento   INT AUTO_INCREMENT PRIMARY KEY,
    tipo_mantenimiento VARCHAR(100) NOT NULL,
    fecha              DATE         NOT NULL,
    descripcion        TEXT,
    ID_vehiculo        INT,
    FOREIGN KEY (ID_vehiculo) REFERENCES vehiculos (ID_vehiculo)
);

-- Tabla para almacenar el Repostaje
CREATE TABLE IF NOT EXISTS repostaje
(
    ID_repostaje    INT AUTO_INCREMENT PRIMARY KEY,
    fecha           DATE           NOT NULL,
    cantidad_litros DECIMAL(10, 2) NOT NULL,
    costo_litro     DECIMAL(10, 2) NOT NULL,
    ID_vehiculo     INT,
    FOREIGN KEY (ID_vehiculo) REFERENCES vehiculos (ID_vehiculo)
);

-- Tabla para almacenar las recargas de vehículos eléctricos
CREATE TABLE IF NOT EXISTS recargas
(
    ID_recarga   INT AUTO_INCREMENT PRIMARY KEY,
    fecha        DATE           NOT NULL,
    cantidad_kWh DECIMAL(10, 2) NOT NULL,
    costo_kWh    DECIMAL(10, 2) NOT NULL,
    ID_vehiculo  INT,
    FOREIGN KEY (ID_vehiculo) REFERENCES vehiculos (ID_vehiculo)
);

-- Tabla para almacenar las alertas
CREATE TABLE IF NOT EXISTS alertas
(
    ID_alerta   INT AUTO_INCREMENT PRIMARY KEY,
    tipo        VARCHAR(100) NOT NULL,
    fecha       DATE         NOT NULL,
    descripcion TEXT,
    ID_usuario  INT,
    mostrar     BOOLEAN      NOT NULL,
    FOREIGN KEY (ID_usuario) REFERENCES usuarios (ID_usuario)
);