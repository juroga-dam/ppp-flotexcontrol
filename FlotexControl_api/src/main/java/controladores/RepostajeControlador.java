package controladores;

import modelos.dao.DAORepostajeImpl;
import modelos.dto.Repostaje;

import java.util.Date;
import java.util.List;

public class RepostajeControlador {

    public void crearRepostaje(Date fecha, double cantidad_litros, double costo_litro, int ID_vehiculo) {
        Repostaje repostaje = new Repostaje();
        repostaje.setFecha(fecha);
        repostaje.setCantidad_litros(cantidad_litros);
        repostaje.setCosto_litro(costo_litro);
        repostaje.setID_vehiculo(ID_vehiculo);
        DAORepostajeImpl dao = new DAORepostajeImpl();
        dao.insertar(repostaje);
    }

    public void actualizarRepostaje(int id, Date fecha, double cantidad_litros, double costo_litro, int ID_vehiculo) {
        Repostaje repostaje = new Repostaje();
        repostaje.setID_repostaje(id);
        repostaje.setFecha(fecha);
        repostaje.setCantidad_litros(cantidad_litros);
        repostaje.setCosto_litro(costo_litro);
        repostaje.setID_vehiculo(ID_vehiculo);
        DAORepostajeImpl dao = new DAORepostajeImpl();
        dao.actualizar(repostaje);
    }

    public void eliminarRepostaje(int id){
        DAORepostajeImpl dao = new DAORepostajeImpl();
        dao.eliminar(id);
    }

    public List<Repostaje> listarRepostajes(){
        DAORepostajeImpl dao = new DAORepostajeImpl();
        return dao.listarRepostajes();
    }

    public Repostaje buscarRepostaje (int id){
        DAORepostajeImpl dao = new DAORepostajeImpl();
        return dao.repostajePorId(id);
    }

    public Repostaje buscarRepostajePorVehiculo (int id){
        DAORepostajeImpl dao = new DAORepostajeImpl();
        return dao.buscarRepostajeVehiculo(id);
    }
}
