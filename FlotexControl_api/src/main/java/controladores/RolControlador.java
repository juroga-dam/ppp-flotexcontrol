package controladores;

import modelos.dao.DAORolImpl;
import modelos.dto.Rol;

import java.util.List;

public class RolControlador {

    public void crearRol(String nombre) {
        Rol rol = new Rol();
        rol.setNombreRol(nombre);
        DAORolImpl dao = new DAORolImpl();
        dao.insertar(rol);
    }

    public void actualizarRol(int id, String nombre){
        Rol rol = new Rol();
        rol.setID_rol(id);
        rol.setNombreRol(nombre);
        DAORolImpl dao = new DAORolImpl();
        dao.actualizar(rol);
    }

    public void eliminarRol(int id){
        DAORolImpl dao = new DAORolImpl();
        dao.eliminar(id);
    }

    public List<Rol> listarRoles(){
        DAORolImpl dao = new DAORolImpl();
        return dao.listarRoles();
        }

}
