package controladores;

import modelos.dao.DAOMantenimientoImpl;
import modelos.dto.Mantenimiento;

import java.sql.Date;
import java.util.List;

public class MantenimientoControlador {

    public void crearMantenimiento(String tipo_mantenimiento, Date fecha, String descripcion, int ID_vehiculo) {
        Mantenimiento mantenimiento = new Mantenimiento();
        mantenimiento.setTipo_mantenimiento(tipo_mantenimiento);
        mantenimiento.setFecha(fecha);
        mantenimiento.setDescripcion(descripcion);
        mantenimiento.setID_vehiculo(ID_vehiculo);
        DAOMantenimientoImpl dao = new DAOMantenimientoImpl();
        dao.insertar(mantenimiento);
    }

    public void actualizarMantenimiento(int id, String tipo_mantenimiento, Date fecha, String descripcion, int ID_vehiculo) {
        Mantenimiento mantenimiento = new Mantenimiento();
        mantenimiento.setID_mantenimiento(id);
        mantenimiento.setTipo_mantenimiento(tipo_mantenimiento);
        mantenimiento.setFecha(fecha);
        mantenimiento.setDescripcion(descripcion);
        mantenimiento.setID_vehiculo(ID_vehiculo);
        DAOMantenimientoImpl dao = new DAOMantenimientoImpl();
        dao.actualizar(mantenimiento);
    }

    public void eliminarMantenimiento(int id){
        DAOMantenimientoImpl dao = new DAOMantenimientoImpl();
        dao.eliminar(id);
    }

    public List<Mantenimiento> listarMantenimientos(){
        DAOMantenimientoImpl dao = new DAOMantenimientoImpl();
        return dao.listarMantenimientos();
    }

    public Mantenimiento buscarMantenimiento(int id){
        DAOMantenimientoImpl dao = new DAOMantenimientoImpl();
        return dao.mantenimientoPorId(id);
    }

}
