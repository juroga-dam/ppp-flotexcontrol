package controladores;

import modelos.dao.DAOVehiculoImpl;
import modelos.dto.Vehiculo;
import java.util.List;

public class VehiculoControlador {
    public  DAOVehiculoImpl dao = new DAOVehiculoImpl();
    public void crearVehiculo(String matricula, String marca, String modelo, int anio, String tipoCombustible, int capacidadCarga, String tipoMotor) {
        Vehiculo vehiculo = new Vehiculo();
        vehiculo.setMatricula(matricula);
        vehiculo.setMarca(marca);
        vehiculo.setModelo(modelo);
        vehiculo.setAnio(anio);
        vehiculo.setTipoCombustible(tipoCombustible);
        vehiculo.setCapacidadCarga(capacidadCarga);
        vehiculo.setTipoMotor(tipoMotor);
        dao.insertar(vehiculo);
        System.out.println("Vehiculo creado");

    }

    public void actualizarVehiculo(int id, String matricula, String marca, String modelo, int anio, String tipoCombustible, int capacidadCarga, String tipoMotor){
        Vehiculo vehiculo = new Vehiculo();
        vehiculo.setID_vehiculo(id);
        vehiculo.setMatricula(matricula);
        vehiculo.setMarca(marca);
        vehiculo.setModelo(modelo);
        vehiculo.setAnio(anio);
        vehiculo.setTipoCombustible(tipoCombustible);
        vehiculo.setCapacidadCarga(capacidadCarga);
        vehiculo.setTipoMotor(tipoMotor);
        dao.actualizar(vehiculo);
        System.out.println("Vehiculo actualizado");
    }

    public void eliminarVehiculo(int id){
        dao.eliminar(id);
    }

    public List<Vehiculo> listarVehiculos(){
        return dao.listarVehiculos();
    }

    public Vehiculo buscarVehiculoId(int id){
        return dao.vehiculoPorId(id);
    }

    public Vehiculo buscarVehiculoMatricula(String matricula){ return dao.vehiculoPorMatricula(matricula); }

    public String getTipoCombustiblePorMatricula(String matricula){
        return dao.getTipoCombustiblePorMatricula(matricula);
    }

}
