package controladores;

import modelos.dao.DAOUsuarioImpl;
import modelos.dto.Usuario;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class UsuarioControlador {

    public void crearUsuario(Usuario usuario) {
        try {
            usuario.setContrasenia(hashPassword(usuario.getContrasenia()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        DAOUsuarioImpl dao = new DAOUsuarioImpl();
        dao.insertar(usuario);
    }

    public void actualizarUsuario(Usuario usuario) {
        DAOUsuarioImpl dao = new DAOUsuarioImpl();
        dao.actualizar(usuario);
    }

     public void eliminarUsuario(int id) {
        DAOUsuarioImpl dao = new DAOUsuarioImpl();
        dao.eliminar(id);
    }

    public List<Usuario> listarUsuarios() {
        DAOUsuarioImpl dao = new DAOUsuarioImpl();
        return dao.listarUsuarios();
    }

    public Usuario buscarUsuario(int id) {
        DAOUsuarioImpl dao = new DAOUsuarioImpl();
        return dao.usuarioPorId(id);
    }

    public Usuario buscarUsuarioPorUsername(String username) {
        DAOUsuarioImpl dao = new DAOUsuarioImpl();
        return dao.usuarioPorUsername(username);
    }

    public boolean procesarUsuario(Usuario usuario) {
        boolean test = (validacionContrasenia(usuario.getContrasenia())) && (validacionNombre(usuario.getNombre()));
        return test;
    }

    private boolean validacionNombre(String nombre) {
        boolean isAlphaNumeric = nombre.length() >= 6 && nombre.chars().allMatch(Character::isLetterOrDigit);
        return isAlphaNumeric;
    }


    private boolean validacionContrasenia(String contrasenia) {
        boolean isAlphaNumeric = contrasenia.length() >= 8 && contrasenia.chars().allMatch(Character::isLetterOrDigit) && !contrasenia.chars().allMatch(Character::isSpaceChar);
        return isAlphaNumeric;
    }


    public static String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");

        // Convierte la contraseña en bytes, actualiza el digest con esos bytes y completa el cálculo del hash
        byte[] encodedhash = digest.digest(password.getBytes(java.nio.charset.StandardCharsets.UTF_8));

        // Convierte el hash en hexadecimal
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < encodedhash.length; i++) {
            String hex = Integer.toHexString(0xff & encodedhash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public Usuario login(String username, String contrasenia) {
        Usuario usuario = buscarUsuarioPorUsername(username);
        Usuario usuarioLogeado = new Usuario();
        usuarioLogeado.setUsername("null");
        usuarioLogeado.setRol(-1);
        usuarioLogeado.setActivo(false);
        try {
            if ((usuario.getActivo()) && (hashPassword(contrasenia).equals(usuario.getContrasenia()))) {
                usuarioLogeado.setID_usuario(usuario.getID_usuario());
                usuarioLogeado.setRol(usuario.getRol());
                usuarioLogeado.setActivo(true);
                return usuarioLogeado;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return usuarioLogeado;
    }
}
