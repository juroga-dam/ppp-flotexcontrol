package controladores;

import modelos.dao.DAORecargaImpl;
import modelos.dto.Recarga;

import java.sql.Date;
import java.util.List;

public class RecargaControlador {

    public void crearRecarga(Date fecha, double cantidad_kWh, double costo_kWh, int ID_vehiculo) {
        Recarga recarga = new Recarga();
        recarga.setFecha(fecha);
        recarga.setCantidad_kWh(cantidad_kWh);
        recarga.setCosto_kWh(costo_kWh);
        recarga.setID_vehiculo(ID_vehiculo);
        DAORecargaImpl dao = new DAORecargaImpl();
        dao.insertar(recarga);
    }

    public void actualizarRecarga(int id, Date fecha, double cantidad_kWh, double costo_kWh, int ID_vehiculo) {
        Recarga recarga = new Recarga();
        recarga.setID_recarga(id);
        recarga.setFecha(fecha);
        recarga.setCantidad_kWh(cantidad_kWh);
        recarga.setCosto_kWh(costo_kWh);
        recarga.setID_vehiculo(ID_vehiculo);
        DAORecargaImpl dao = new DAORecargaImpl();
        dao.actualizar(recarga);
    }

    public void eliminarRecarga(int id){
        DAORecargaImpl dao = new DAORecargaImpl();
        dao.eliminar(id);
    }

    public List<Recarga> listarRecargas(){
        DAORecargaImpl dao = new DAORecargaImpl();
        return dao.listarRecargas();
    }

    public Recarga buscarRecarga(int id){
        DAORecargaImpl dao = new DAORecargaImpl();
        return dao.recargaPorId(id);
    }

}
