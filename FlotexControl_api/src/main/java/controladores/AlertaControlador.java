package controladores;

import modelos.dao.DAOAlertaImpl;
import modelos.dto.Alerta;

import java.util.Date;
import java.util.List;

public class AlertaControlador {

    public void crearAlerta(String tipo, Date fecha, String descripcion, int ID_usuario) {
        Alerta alerta = new Alerta();
        alerta.setTipo(tipo);
        alerta.setFecha(fecha);
        alerta.setDescripcion(descripcion);
        alerta.setID_usuario(ID_usuario);
        alerta.setMostrar(true);
        DAOAlertaImpl dao = new DAOAlertaImpl();
        dao.insertar(alerta);
    }

    public void actualizarAlerta(int id, String tipo, Date fecha, String descripcion, int ID_usuario) {
        Alerta alerta = new Alerta();
        alerta.setID_alerta(id);
        alerta.setTipo(tipo);
        alerta.setFecha(fecha);
        alerta.setDescripcion(descripcion);
        alerta.setID_usuario(ID_usuario);
        alerta.setMostrar(true);
        DAOAlertaImpl dao = new DAOAlertaImpl();
        dao.actualizar(alerta);
    }

    public void eliminarAlerta(int id){
        DAOAlertaImpl dao = new DAOAlertaImpl();
        dao.eliminar(id);
    }

    public List<Alerta> listarAlertas(){
        DAOAlertaImpl dao = new DAOAlertaImpl();
        return dao.listarAlertas();
    }

    public Alerta buscarAlerta(int id){
        DAOAlertaImpl dao = new DAOAlertaImpl();
        return dao.alertaPorId(id);
    }

}
