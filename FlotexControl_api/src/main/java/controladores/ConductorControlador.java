package controladores;

import modelos.dao.DAOConductorImpl;
import modelos.dto.Conductor;

import java.util.List;

public class ConductorControlador {

    public void crearConductor(String nombre, String apellido, String licencia_conducir) {
        Conductor conductor = new Conductor();
        conductor.setNombre(nombre);
        conductor.setApellido(apellido);
        conductor.setLicencia_conducir(licencia_conducir);
        DAOConductorImpl dao = new DAOConductorImpl();
        dao.insertar(conductor);
        System.out.println("Conductor creado");

    }

    public void actualizarConductor(int id, String nombre, String apellido, String licencia_conducir){
        Conductor conductor = new Conductor();
        conductor.setID_conductor(id);
        conductor.setNombre(nombre);
        conductor.setApellido(apellido);
        conductor.setLicencia_conducir(licencia_conducir);
        DAOConductorImpl dao = new DAOConductorImpl();
        dao.actualizar(conductor);
    }

    public void eliminarConductor(int id){
        DAOConductorImpl dao = new DAOConductorImpl();
        dao.eliminar(id);
    }

    public List<Conductor> listarConductores(){
        DAOConductorImpl dao = new DAOConductorImpl();
        return dao.listarConductores();
    }

    public Conductor buscarConductor(int id){
        DAOConductorImpl dao = new DAOConductorImpl();
        return dao.conductorPorId(id);
    }

    public void asignarVehiculo(int id_conductor, int id_vehiculo){
        DAOConductorImpl dao = new DAOConductorImpl();
        dao.asignarVehiculo(id_conductor, id_vehiculo);
    }
}
