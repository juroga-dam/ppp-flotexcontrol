package Vista.tableModels;

import modelos.dto.Vehiculo;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class VehiculosTabletModel extends AbstractTableModel {
    private List<Vehiculo> listaVehiculos;
    private String[] columnas = {"Matricula", "Marca", "Modelo", "Año", "Tipo de combustible", "Capacidad"};

    public VehiculosTabletModel(List<Vehiculo> vehiculos) {
        this.listaVehiculos = vehiculos;
    }

    @Override
    public int getRowCount() {
        return listaVehiculos.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: return listaVehiculos.get(rowIndex).getMatricula();
            case 1: return listaVehiculos.get(rowIndex).getMarca();
            case 2: return listaVehiculos.get(rowIndex).getModelo();
            case 3: return listaVehiculos.get(rowIndex).getAnio();
            case 4: return listaVehiculos.get(rowIndex).getTipoCombustible();
            case 5: return listaVehiculos.get(rowIndex).getCapacidadCarga();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }
}
