package Vista.tableModels;

import modelos.dto.Recarga;
import java.util.stream.Collectors;
import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.List;

public class RecargaTableModel extends AbstractTableModel {

    private List<Recarga> listaRecarga; // Lista completa de datos
    private List<Recarga> listaFiltrada; // Datos filtrados que se mostrarán
    private final String[] columnas = {"id", "Fecha", "kWh", "Costo por kWh"};

    public RecargaTableModel(List<Recarga> repostajes) {
        this.listaRecarga = repostajes;
        this.listaFiltrada = repostajes;
    }

    public void setFiltroPorMatricula(int idVehiculo) {
        // Actualiza la lista filtrada con los elementos que coincidan con el filtro
        this.listaFiltrada = listaRecarga.stream()
                .filter(repostaje -> repostaje.getID_vehiculo() == idVehiculo)
                .collect(Collectors.toList());
        fireTableDataChanged(); // Notifica a la tabla que los datos han cambiado
    }

    @Override
    public int getRowCount() {
        return listaFiltrada.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Recarga recarga = listaFiltrada.get(rowIndex);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        switch (columnIndex) {
            case 0: return recarga.getID_recarga();
            case 1: return dateFormat.format(recarga.getFecha());
            case 2: return recarga.getCantidad_kWh();
            case 3: return recarga.getCosto_kWh();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }
}