package Vista.tableModels;

import modelos.dto.Alerta;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.List;

public class AlertaTableModel extends AbstractTableModel {
    private final List<Alerta> listaAlertas;
    private final String[] columnas = {"Fecha", "Descripción"};

    public AlertaTableModel(List<Alerta> alertas) {
        this.listaAlertas = alertas;
    }

    @Override
    public int getRowCount() {
        return listaAlertas.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Alerta alerta = listaAlertas.get(rowIndex);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        switch (columnIndex) {
            case 0: return dateFormat.format(alerta.getFecha());
            case 1: return alerta.getDescripcion();
            //default: return null;
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }
}
