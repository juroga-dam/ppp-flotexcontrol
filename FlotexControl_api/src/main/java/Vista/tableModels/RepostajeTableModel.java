package Vista.tableModels;

import modelos.dto.Repostaje;
import java.util.stream.Collectors;
import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.List;

public class RepostajeTableModel extends AbstractTableModel {

    private List<Repostaje> listaRepostajes; // Lista completa de datos
    private List<Repostaje> listaFiltrada; // Datos filtrados que se mostrarán
    private final String[] columnas = {"id", "Fecha", "Cantidad de litros", "Costo por litro"};

    public RepostajeTableModel(List<Repostaje> repostajes) {
        this.listaRepostajes = repostajes;
        this.listaFiltrada = repostajes;
    }

    public void setFiltroPorMatricula(int idVehiculo) {
        // Actualiza la lista filtrada con los elementos que coincidan con el filtro
        this.listaFiltrada = listaRepostajes.stream()
                .filter(repostaje -> repostaje.getID_vehiculo() == idVehiculo)
                .collect(Collectors.toList());
        fireTableDataChanged(); // Notifica a la tabla que los datos han cambiado
    }

    @Override
    public int getRowCount() {
        return listaFiltrada.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Repostaje repostaje = listaFiltrada.get(rowIndex);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        switch (columnIndex) {
            case 0: return repostaje.getID_repostaje();
            case 1: return dateFormat.format(repostaje.getFecha());
            case 2: return repostaje.getCantidad_litros();
            case 3: return repostaje.getCosto_litro();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }
}

