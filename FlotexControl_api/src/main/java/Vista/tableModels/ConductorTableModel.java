package Vista.tableModels;

import modelos.dto.Conductor;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ConductorTableModel extends AbstractTableModel {
    private final List<Conductor> listaConductores;
    private final String[] columnas = {"id","Nombre", "Apellidos", "Licencia", "vehiculo"};

    public ConductorTableModel(List<Conductor> conductores) {
        this.listaConductores = conductores;
    }

    @Override
    public int getRowCount() {
        return listaConductores.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: return listaConductores.get(rowIndex).getID_conductor();
            case 1: return listaConductores.get(rowIndex).getNombre();
            case 2: return listaConductores.get(rowIndex).getApellido();
            case 3: return listaConductores.get(rowIndex).getLicencia_conducir();
            case 4: return listaConductores.get(rowIndex).getID_vehiculo();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }
}
