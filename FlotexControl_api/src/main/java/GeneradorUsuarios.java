/*import controladores.UsuarioControlador;
import modelos.dto.Usuario;

public class GeneradorUsuarios {

    public static void main(String[] args) {
        UsuarioControlador controladorUsuario = null;


        Usuario userAdmin = new Usuario();
        userAdmin.setID_usuario(1);
        userAdmin.setNombre("Adminsitrador");
        userAdmin.setApellidos("Admin");
        userAdmin.setUsername("admin");
        userAdmin.setContrasenia("admin");
        userAdmin.setRol(1);
        //Llamada al método para crear el usuario
        controladorUsuario.crearUsuario(userAdmin);

        Usuario user = new Usuario();
        user.setID_usuario(2);
        user.setNombre("Jose Luis");
        user.setApellidos("Perez");
        user.setUsername("pepelu");
        user.setContrasenia("password2");
        user.setRol(1);
        //Llamada al método para crear el usuario
        controladorUsuario.crearUsuario(user);

    }

}*/

import controladores.UsuarioControlador;
import modelos.dao.DAOUsuarioImpl;
import modelos.dto.Usuario;

import java.text.Normalizer;
import java.util.Random;

public class GeneradorUsuarios {

    public static void main(String[] args) {
        UsuarioControlador controladorUsuario = new UsuarioControlador();

        DAOUsuarioImpl daoUsuario = new DAOUsuarioImpl();
        Usuario encontrado = daoUsuario.usuarioPorUsername("admin");
        if (encontrado == null) {
            // Crear usuario admin
            Usuario userAdmin = new Usuario();
            userAdmin.setID_usuario(1);
            userAdmin.setNombre("Administrador");
            userAdmin.setApellidos("Admin");
            userAdmin.setUsername("admin");
            userAdmin.setContrasenia("admin");
            userAdmin.setRol(1);
            // Llamada al método para crear el usuario
            controladorUsuario.crearUsuario(userAdmin);
        }

        // Generar 10 usuarios adicionales
        for (int i = 2; i <= 11; i++) {
            Usuario user = new Usuario();
            user.setID_usuario(i);
            user.setNombre(generarNombreAleatorio());
            user.setApellidos(generarApellidosAleatorios());
            user.setUsername(generarUsername(user.getNombre(), user.getApellidos()));
            user.setContrasenia(generarContraseniaAleatoria());
            user.setRol(generarRolAleatorio());
            // Llamada al método para crear el usuario
            controladorUsuario.crearUsuario(user);
        }
    }

    // Método para generar nombres aleatorios
    private static String generarNombreAleatorio() {
        String[] nombres = {"Juan", "María", "Carlos", "Laura", "Pedro", "Ana", "Diego", "Sofía", "José", "Elena"};
        Random rand = new Random();
        return nombres[rand.nextInt(nombres.length)];
    }

    // Método para generar apellidos aleatorios
    private static String generarApellidosAleatorios() {
        String[] apellidos = {"García", "López", "Martínez", "Rodríguez", "Fernández", "González", "Pérez", "Sánchez", "Ramírez", "Torres"};
        Random rand = new Random();
        return apellidos[rand.nextInt(apellidos.length)];
    }

    // Método para reemplazar caracteres con tilde por el mismo caracter sin tilde
    private static String reemplazarTildes(String texto) {
        String textoSinTildes = Normalizer.normalize(texto, Normalizer.Form.NFD);
        textoSinTildes = textoSinTildes.replaceAll("[^\\p{ASCII}]", "");
        return textoSinTildes;
    }

    // Método para generar username relacionado con nombre y apellidos, sin caracteres con tilde
    private static String generarUsername(String nombre, String apellido) {
        // Reemplazar caracteres con tilde por el mismo caracter sin tilde
        String nombreSinTilde = reemplazarTildes(nombre.toLowerCase());
        String apellidoSinTilde = reemplazarTildes(apellido.toLowerCase());
        // Concatenar nombre y apellido sin tildes
        return nombreSinTilde.charAt(0) + apellidoSinTilde;
    }

/*    private static String generarUsername(String nombre, String apellidos) {
        return nombre.toLowerCase().charAt(0) + apellidos.toLowerCase().replace(" ", "");
    }
*/

    // Método para generar una contraseña alfanumérica de 12 caracteres
    private static String generarContraseniaAleatoria() {
        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random rand = new Random();
        StringBuilder contrasenia = new StringBuilder();
        for (int i = 0; i < 12; i++) {
            contrasenia.append(caracteres.charAt(rand.nextInt(caracteres.length())));
        }
        return contrasenia.toString();
    }

    // Método para generar un rol aleatorio entre 1 y 3
    private static int generarRolAleatorio() {
        Random rand = new Random();
        return rand.nextInt(3) + 1;
    }
}
