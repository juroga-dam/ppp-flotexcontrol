package modelos.dao;

import modelos.dto.Mantenimiento;
import modelos.interfaces.DAOMantenimiento;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAOMantenimientoImpl extends Conexion implements DAOMantenimiento{

    @Override
    public void insertar(Mantenimiento mantenimiento) {
        try {
            this.conectar();
            String sql = "INSERT INTO mantenimientos (tipo_mantenimiento, fecha, descripcion, ID_vehiculo) VALUES(?, ?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, mantenimiento.getTipo_mantenimiento());
            st.setDate(2, mantenimiento.getFecha());
            st.setString(3, mantenimiento.getDescripcion());
            st.setInt(4, mantenimiento.getID_vehiculo());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Mantenimiento mantenimiento) {
        try {
            this.conectar();
            String sql = "UPDATE mantenimientos SET tipo_mantenimiento = ?, fecha = ?, descripcion = ?, ID_vehiculo = ? WHERE ID_mantenimiento = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, mantenimiento.getTipo_mantenimiento());
            st.setDate(2, mantenimiento.getFecha());
            st.setString(3, mantenimiento.getDescripcion());
            st.setInt(4, mantenimiento.getID_vehiculo());
            st.setInt(5, mantenimiento.getID_mantenimiento());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM mantenimientos WHERE ID_mantenimiento = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Mantenimiento> listarMantenimientos() {
        List<Mantenimiento> listaMantenimientos = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM mantenimientos";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaMantenimientos = new ArrayList<>();
            while (rs.next()) {
                Mantenimiento mantenimiento = new Mantenimiento();
                mantenimiento.setID_mantenimiento(rs.getInt("ID_mantenimiento"));
                mantenimiento.setTipo_mantenimiento(rs.getString("tipo_mantenimiento"));
                mantenimiento.setFecha(rs.getDate("fecha"));
                mantenimiento.setDescripcion(rs.getString("descripcion"));
                mantenimiento.setID_vehiculo(rs.getInt("ID_vehiculo"));
                listaMantenimientos.add(mantenimiento);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaMantenimientos;
    }

    public Mantenimiento mantenimientoPorId(int id) {
        Mantenimiento mantenimiento = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM mantenimientos WHERE ID_mantenimiento = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                mantenimiento = new Mantenimiento();
                mantenimiento.setID_mantenimiento(rs.getInt("ID_mantenimiento"));
                mantenimiento.setTipo_mantenimiento(rs.getString("tipo_mantenimiento"));
                mantenimiento.setFecha(rs.getDate("fecha"));
                mantenimiento.setDescripcion(rs.getString("descripcion"));
                mantenimiento.setID_vehiculo(rs.getInt("ID_vehiculo"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return mantenimiento;
    }
}
