package modelos.dao;

import modelos.dto.Vehiculo;
import modelos.interfaces.DAOVehiculo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAOVehiculoImpl extends Conexion implements DAOVehiculo {

    @Override
    public void insertar(Vehiculo vehiculo) {
        try {
            this.conectar();
            String sql = "INSERT INTO vehiculos (matricula, marca, modelo, anio, tipoCombustible, capacidadCarga, tipoMotor) VALUES(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, vehiculo.getMatricula());
            st.setString(2, vehiculo.getMarca());
            st.setString(3, vehiculo.getModelo());
            st.setInt(4, vehiculo.getAnio());
            st.setString(5, vehiculo.getTipoCombustible());
            st.setDouble(6, vehiculo.getCapacidadCarga());
            st.setString(7, vehiculo.getTipoMotor());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Vehiculo vehiculo) {
        try {
            this.conectar();
            String sql = "UPDATE vehiculos SET matricula = ?, marca = ?, modelo = ?, anio = ?, tipoCombustible = ?, capacidadCarga = ?, tipoMotor = ? WHERE ID_vehiculo = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, vehiculo.getMatricula());
            st.setString(2, vehiculo.getMarca());
            st.setString(3, vehiculo.getModelo());
            st.setInt(4, vehiculo.getAnio());
            st.setString(5, vehiculo.getTipoCombustible());
            st.setDouble(6, vehiculo.getCapacidadCarga());
            st.setString(7, vehiculo.getTipoMotor());
            st.setInt(8, vehiculo.getID_vehiculo());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM vehiculos WHERE ID_vehiculo = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Vehiculo> listarVehiculos() {
        List<Vehiculo> listaVehiculos = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM vehiculos";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaVehiculos = new ArrayList<>();
            while (rs.next()) {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.setID_vehiculo(rs.getInt("ID_vehiculo"));
                vehiculo.setMatricula(rs.getString("matricula"));
                vehiculo.setMarca(rs.getString("marca"));
                vehiculo.setModelo(rs.getString("modelo"));
                vehiculo.setAnio(rs.getInt("anio"));
                vehiculo.setTipoCombustible(rs.getString("tipoCombustible"));
                vehiculo.setCapacidadCarga(rs.getInt("capacidadCarga"));
                vehiculo.setTipoMotor(rs.getString("tipoMotor"));
                listaVehiculos.add(vehiculo);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaVehiculos;
    }

    public Vehiculo vehiculoPorId(int id) {
        Vehiculo vehiculo = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM vehiculos WHERE ID_vehiculo = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                vehiculo = new Vehiculo();
                vehiculo.setID_vehiculo(rs.getInt("ID_vehiculo"));
                vehiculo.setMatricula(rs.getString("matricula"));
                vehiculo.setMarca(rs.getString("marca"));
                vehiculo.setModelo(rs.getString("modelo"));
                vehiculo.setAnio(rs.getInt("anio"));
                vehiculo.setTipoCombustible(rs.getString("tipoCombustible"));
                vehiculo.setCapacidadCarga(rs.getInt("capacidadCarga"));
                vehiculo.setTipoMotor(rs.getString("tipoMotor"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return vehiculo;
    }

    public Vehiculo vehiculoPorMatricula(String matricula) {
        Vehiculo vehiculo = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM vehiculos WHERE matricula = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, matricula);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                vehiculo = new Vehiculo();
                vehiculo.setID_vehiculo(rs.getInt("ID_vehiculo"));
                vehiculo.setMatricula(rs.getString("matricula"));
                vehiculo.setMarca(rs.getString("marca"));
                vehiculo.setModelo(rs.getString("modelo"));
                vehiculo.setAnio(rs.getInt("anio"));
                vehiculo.setTipoCombustible(rs.getString("tipoCombustible"));
                vehiculo.setCapacidadCarga(rs.getInt("capacidadCarga"));
                vehiculo.setTipoMotor(rs.getString("tipoMotor"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return vehiculo;
    }

    public void resetIDTable() {
        try {
            this.conectar();
            String sql = "ALTER TABLE vehiculos AUTO_INCREMENT = 1";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    public String getTipoCombustiblePorMatricula(String matricula) {
        String tipoCombustible = null;
        try {
            this.conectar();
            String sql = "SELECT tipoCombustible FROM vehiculos WHERE matricula = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, matricula);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                tipoCombustible = rs.getString("tipoCombustible");
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return tipoCombustible;
    }
}
