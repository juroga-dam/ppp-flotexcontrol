package modelos.dao;

import modelos.dto.Rol;
import modelos.interfaces.DAORol;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAORolImpl extends Conexion implements DAORol{

    @Override
    public void insertar(Rol rol) {
        try {
            this.conectar();
            String sql = "INSERT INTO roles (nombreRol) VALUES(?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, rol.getNombreRol());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Rol rol) {
        try {
            this.conectar();
            String sql = "UPDATE roles SET nombreRol = ? WHERE ID_rol = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, rol.getNombreRol());
            st.setInt(2, rol.getID_rol());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM roles WHERE nombreRol = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Rol> listarRoles() {
        List<Rol> listaRoles = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM roles";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaRoles = new ArrayList<>();
            while (rs.next()) {
                Rol rol = new Rol();
                rol.setID_rol(rs.getInt("ID_rol"));
                rol.setNombreRol(rs.getString("nombreRol"));
                listaRoles.add(rol);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaRoles;
    }

}
