package modelos.dao;

import modelos.interfaces.DAOUsuario;
import modelos.dto.Usuario;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAOUsuarioImpl extends Conexion implements DAOUsuario{

    @Override
    public void insertar(Usuario usuario) {
        try {
            this.conectar();
            String sql = "INSERT INTO usuarios (nombre, apellidos, username, contrasenia, rol) VALUES(?, ?, ?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, usuario.getNombre());
            st.setString(2, usuario.getApellidos());
            st.setString(3, usuario.getUsername());
            st.setString(4, usuario.getContrasenia());
            st.setInt(5, usuario.getRol());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Usuario usuario) {
        try {
            this.conectar();
            String sql = "UPDATE usuarios SET nombre = ?, contrasenia = ?, rol = ? WHERE ID_usuario = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, usuario.getNombre());
            st.setString(2, usuario.getContrasenia());
            st.setInt(3, usuario.getRol());
            st.setInt(4, usuario.getID_usuario());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM usuarios WHERE ID_usuario = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Usuario> listarUsuarios() {
        List<Usuario> listaUsuarios = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM usuarios";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaUsuarios = new ArrayList<>();
            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setID_usuario(rs.getInt("id_usuario"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setUsername(rs.getString("username"));
                usuario.setRol(rs.getInt("rol"));
                listaUsuarios.add(usuario);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaUsuarios;
    }

    public Usuario usuarioPorId(int id) {
        Usuario usuario = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM usuarios WHERE ID_usuario = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                usuario = new Usuario();
                usuario.setID_usuario(rs.getInt("id_usuario"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setUsername(rs.getString("username"));
                usuario.setRol(rs.getInt("rol"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return usuario;
    }

    public Usuario usuarioPorUsername(String username) {
        Usuario usuario = new Usuario();
        usuario.setID_usuario(0);
        usuario.setRol(0);
        usuario.setActivo(false);
        try {
            this.conectar();
            String sql = "SELECT * FROM usuarios WHERE username = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                usuario = new Usuario();
                usuario.setID_usuario(rs.getInt("id_usuario"));
                usuario.setContrasenia(rs.getString("contrasenia"));
                usuario.setRol(rs.getInt("rol"));
                usuario.setActivo(true);
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return usuario;
    }
}
