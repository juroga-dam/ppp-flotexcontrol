package modelos.dao;

import modelos.dto.Repostaje;
import modelos.interfaces.DAORepostaje;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAORepostajeImpl extends Conexion implements DAORepostaje {

    @Override
    public void insertar(Repostaje repostaje) {
        try {
            this.conectar();
            String sql = "INSERT INTO repostaje (fecha, cantidad_litros, costo_litro, ID_vehiculo) VALUES(?, ?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setDate(1, new java.sql.Date(repostaje.getFecha().getTime()));
            st.setDouble(2, repostaje.getCantidad_litros());
            st.setDouble(3, repostaje.getCosto_litro());
            st.setInt(4, repostaje.getID_vehiculo());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Repostaje repostaje) {
        try {
            this.conectar();
            String sql = "UPDATE repostaje SET fecha = ?, cantidad_litros = ?, costo_litro = ?, ID_vehiculo = ? WHERE ID_repostaje = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setDate(1, new java.sql.Date(repostaje.getFecha().getTime()));
            st.setDouble(2, repostaje.getCantidad_litros());
            st.setDouble(3, repostaje.getCosto_litro());
            st.setInt(4, repostaje.getID_vehiculo());
            st.setInt(5, repostaje.getID_repostaje());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM repostaje WHERE ID_repostaje = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Repostaje> listarRepostajes() {
        List<Repostaje> listaRepostajes = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM repostaje";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaRepostajes = new ArrayList<>();
            while (rs.next()) {
                Repostaje repostaje = new Repostaje();
                repostaje.setID_repostaje(rs.getInt("ID_repostaje"));
                repostaje.setFecha(rs.getDate("fecha"));
                repostaje.setCantidad_litros(rs.getDouble("cantidad_litros"));
                repostaje.setCosto_litro(rs.getDouble("costo_litro"));
                repostaje.setID_vehiculo(rs.getInt("ID_vehiculo"));
                listaRepostajes.add(repostaje);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaRepostajes;
    }

    public Repostaje repostajePorId(int id) {
        Repostaje repostaje = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM repostaje WHERE ID_repostaje = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                repostaje = new Repostaje();
                repostaje.setID_repostaje(rs.getInt("ID_repostaje"));
                repostaje.setFecha(rs.getDate("fecha"));
                repostaje.setCantidad_litros(rs.getDouble("cantidad_litros"));
                repostaje.setCosto_litro(rs.getDouble("costo_litro"));
                repostaje.setID_vehiculo(rs.getInt("ID_vehiculo"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return repostaje;
    }

    public Repostaje buscarRepostajeVehiculo(int id) {
        Repostaje repostaje = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM repostaje WHERE ID_vehiculo = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                repostaje = new Repostaje();
                repostaje.setID_repostaje(rs.getInt("ID_repostaje"));
                repostaje.setFecha(rs.getDate("fecha"));
                repostaje.setCantidad_litros(rs.getDouble("cantidad_litros"));
                repostaje.setCosto_litro(rs.getDouble("costo_litro"));
                repostaje.setID_vehiculo(rs.getInt("ID_vehiculo"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return repostaje;
    }
}
