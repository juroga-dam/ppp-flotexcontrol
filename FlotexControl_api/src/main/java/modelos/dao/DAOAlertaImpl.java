package modelos.dao;

import modelos.dto.Alerta;
import modelos.interfaces.DAOAlerta;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAOAlertaImpl extends Conexion implements DAOAlerta {

    @Override
    public void insertar(Alerta alerta) {
        try {
            this.conectar();
            String sql = "INSERT INTO alertas (tipo, fecha, descripcion, ID_usuario, mostrar) VALUES(?, ?, ?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, alerta.getTipo());
            st.setDate(2, new java.sql.Date(alerta.getFecha().getTime()));
            st.setString(3, alerta.getDescripcion());
            st.setInt(4, alerta.getID_usuario());
            st.setBoolean(5, alerta.getMostrar());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Alerta alerta) {
        try {
            this.conectar();
            String sql = "UPDATE alertas SET tipo = ?, fecha = ?, descripcion = ?, ID_usuario = ?, mostrar = ? WHERE ID_alerta = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, alerta.getTipo());
            st.setDate(2, new java.sql.Date(alerta.getFecha().getTime()));
            st.setString(3, alerta.getDescripcion());
            st.setInt(4, alerta.getID_usuario());
            st.setBoolean(5, alerta.getMostrar());
            st.setInt(6, alerta.getID_alerta());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "UPDATE alertas SET mostrar = ? WHERE ID_alerta = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setBoolean(1, false);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Alerta> listarAlertas() {
        List<Alerta> listaAlertas = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM alertas";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaAlertas = new ArrayList<>();
            while (rs.next()) {
                Alerta alerta = new Alerta();
                alerta.setID_alerta(rs.getInt("ID_alerta"));
                alerta.setTipo(rs.getString("tipo"));
                alerta.setFecha(rs.getDate("fecha"));
                alerta.setDescripcion(rs.getString("descripcion"));
                alerta.setID_usuario(rs.getInt("ID_usuario"));
                alerta.setMostrar(rs.getBoolean("mostrar"));
                listaAlertas.add(alerta);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaAlertas;
    }

    public Alerta alertaPorId(int id) {
        Alerta alerta = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM alertas WHERE ID_alerta = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                alerta = new Alerta();
                alerta.setID_alerta(rs.getInt("ID_alerta"));
                alerta.setTipo(rs.getString("tipo"));
                alerta.setFecha(rs.getDate("fecha"));
                alerta.setDescripcion(rs.getString("descripcion"));
                alerta.setID_usuario(rs.getInt("ID_usuario"));
                alerta.setMostrar(rs.getBoolean("mostrar"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return alerta;
    }

    public void resetIDTable() {
        try {
            this.conectar();
            String sql = "ALTER TABLE alertas AUTO_INCREMENT = 1";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }
}
