package modelos.dao;

import modelos.dto.Recarga;
import modelos.interfaces.DAORecarga;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAORecargaImpl extends Conexion implements DAORecarga{

    @Override
    public void insertar(Recarga recarga) {
        try {
            this.conectar();
            String sql = "INSERT INTO recargas (fecha, cantidad_kWh, costo_kWh, ID_vehiculo) VALUES(?, ?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setDate(1, recarga.getFecha());
            st.setDouble(2, recarga.getCantidad_kWh());
            st.setDouble(3, recarga.getCosto_kWh());
            st.setInt(4, recarga.getID_vehiculo());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Recarga recarga) {
        try {
            this.conectar();
            String sql = "UPDATE recargas SET fecha = ?, cantidad_kWh = ?, costo_kWh = ?, ID_vehiculo = ? WHERE ID_recarga = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setDate(1, recarga.getFecha());
            st.setDouble(2, recarga.getCantidad_kWh());
            st.setDouble(3, recarga.getCosto_kWh());
            st.setInt(4, recarga.getID_vehiculo());
            st.setInt(5, recarga.getID_recarga());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM recargas WHERE ID_recarga = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Recarga> listarRecargas() {
        List<Recarga> listaRecargas = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM recargas";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaRecargas = new ArrayList<>();
            while (rs.next()) {
                Recarga recarga = new Recarga();
                recarga.setID_recarga(rs.getInt("ID_recarga"));
                recarga.setFecha(rs.getDate("fecha"));
                recarga.setCantidad_kWh(rs.getDouble("cantidad_kWh"));
                recarga.setCosto_kWh(rs.getDouble("costo_kWh"));
                recarga.setID_vehiculo(rs.getInt("ID_vehiculo"));
                listaRecargas.add(recarga);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaRecargas;
    }

    public Recarga recargaPorId(int id) {
        Recarga recarga = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM recargas WHERE ID_recarga = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                recarga = new Recarga();
                recarga.setID_recarga(rs.getInt("ID_recarga"));
                recarga.setFecha(rs.getDate("fecha"));
                recarga.setCantidad_kWh(rs.getDouble("cantidad_kWh"));
                recarga.setCosto_kWh(rs.getDouble("costo_kWh"));
                recarga.setID_vehiculo(rs.getInt("ID_vehiculo"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return recarga;
    }
}
