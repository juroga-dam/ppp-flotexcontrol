package modelos.dao;

import modelos.interfaces.DAOConductor;
import modelos.dto.Conductor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAOConductorImpl extends Conexion implements DAOConductor {

    @Override
    public void insertar(Conductor conductor) {
        try {
            this.conectar();
            String sql = "INSERT INTO conductores (nombre, apellido, licencia_conducir) VALUES(?, ?, ?)";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, conductor.getNombre());
            st.setString(2, conductor.getApellido());
            st.setString(3, conductor.getLicencia_conducir());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Conductor conductor) {
        try {
            this.conectar();
            String sql = "UPDATE conductores SET nombre = ?, apellido = ?, licencia_conducir = ? WHERE ID_conductor = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, conductor.getNombre());
            st.setString(2, conductor.getApellido());
            st.setString(3, conductor.getLicencia_conducir());
            st.setInt(4, conductor.getID_conductor());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            this.conectar();
            String sql = "DELETE FROM conductores WHERE ID_conductor = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    @Override
    public List<Conductor> listarConductores() {
        List<Conductor> listaConductores = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM conductores";
            PreparedStatement st = conexion.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            listaConductores = new ArrayList<>();
            while (rs.next()) {
                Conductor conductor = new Conductor();
                conductor.setID_conductor(rs.getInt("ID_conductor"));
                conductor.setNombre(rs.getString("nombre"));
                conductor.setApellido(rs.getString("apellido"));
                conductor.setLicencia_conducir(rs.getString("licencia_conducir"));
                listaConductores.add(conductor);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
        return listaConductores;
    }

    @Override
    public Conductor conductorPorId(int id) {
        Conductor conductor = null;
        try {
            this.conectar();
            String sql = "SELECT * FROM conductores WHERE ID_conductor = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                conductor = new Conductor();
                conductor.setID_conductor(rs.getInt("ID_conductor"));
                conductor.setNombre(rs.getString("nombre"));
                conductor.setApellido(rs.getString("apellido"));
                conductor.setLicencia_conducir(rs.getString("licencia_conducir"));
                conductor.setID_vehiculo(rs.getInt("ID_vehiculo"));
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            this.desconectar();
        }
        return conductor;
    }

    @Override
    public void asignarVehiculo(int idConductor, int idVehiculo) {
        try {
            this.conectar();
            String sql = "UPDATE conductores SET ID_vehiculo = ? WHERE ID_conductor = ?";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setInt(1, idVehiculo);
            st.setInt(2, idConductor);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

    public void resetIDTable() {
        try {
            this.conectar();
            String sql = "ALTER TABLE conductores AUTO_INCREMENT = 1";
            PreparedStatement st = conexion.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            this.desconectar();
        }
    }

}
