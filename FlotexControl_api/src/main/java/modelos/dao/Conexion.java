/*
package modelos.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;

public class Conexion {

    protected Connection conexion;
    //private final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String URL = "jdbc:mariadb://200.234.235.112:61389/flotexcontrol";
    private final String USER = "flotex";
    private final String PASS = "laNgUerISTeA";

    public void conectar() {
        try {
            conexion = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLNonTransientConnectionException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void desconectar() {
        try {
            if (conexion != null) {
                if (!conexion.isClosed()) {
                    conexion.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
*/

package modelos.dao;

import javax.swing.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.io.InputStream;

public class Conexion {

    private JOptionPane jOptionPane = new JOptionPane();
    protected Connection conexion;
    private String url;
    private String user;
    private String pass;

    public Conexion() {
        cargarPropiedades();
    }

    private void cargarPropiedades() {
        Properties prop = new Properties();
        String nombreArchivoPropiedades = "application-" + System.getProperty("environment", "dev") + ".properties";
        try (InputStream input = getClass().getClassLoader().getResourceAsStream(nombreArchivoPropiedades)) {
            if (input == null) {
                throw new RuntimeException("No se pudo encontrar el archivo de configuración: " + nombreArchivoPropiedades);
            }
            prop.load(input);
            this.url = prop.getProperty("db.url");
            this.user = prop.getProperty("db.user");
            this.pass = prop.getProperty("db.password");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error al cargar las propiedades de la base de datos", ex);
        }
    }

    public void conectar() {

        try {
            // Utilizar las propiedades cargadas para establecer la conexión
            conexion = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            System.out.println("Error al conectar a la base de datos");
            jOptionPane.showMessageDialog(null, "Error al conectar a la base de datos");
            //e.printStackTrace();
            //throw new RuntimeException("Error al conectar a la base de datos", e);
        } catch (Exception e) {
            jOptionPane.showMessageDialog(null, "Error al conectar a la base de datos");
            System.out.println("Error al conectar a la base de datos");
            //e.printStackTrace();
            //throw new RuntimeException("Error al conectar a la base de datos", e);
        }
    }

    public void desconectar() {
        try {
            if (conexion != null) {
                if (!conexion.isClosed()) {
                    conexion.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public PreparedStatement prepareStatement(String sql) {
        try {
            return conexion.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error al preparar la sentencia SQL", e);
        }
    }*/
}