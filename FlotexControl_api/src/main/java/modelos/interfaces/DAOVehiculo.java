package modelos.interfaces;

import modelos.dto.Vehiculo;
import java.util.List;

public interface DAOVehiculo {
    public void insertar(Vehiculo vehiculo);
    public void actualizar(Vehiculo vehiculo);
    public void eliminar(int id);
    public List<Vehiculo> listarVehiculos();
    public Vehiculo vehiculoPorId(int id);
    public Vehiculo vehiculoPorMatricula(String matricula);

}
