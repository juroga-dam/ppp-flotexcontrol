package modelos.interfaces;

import modelos.dto.Alerta;

import java.util.List;

public interface DAOAlerta {
    public void insertar(Alerta alerta);
    public void actualizar(Alerta alerta);
    public void eliminar(int id);
    public List<Alerta> listarAlertas();
    public Alerta alertaPorId(int id);
}
