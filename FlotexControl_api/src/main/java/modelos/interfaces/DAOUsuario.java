package modelos.interfaces;

import modelos.dto.Usuario;

import java.util.List;

public interface DAOUsuario {
    public void insertar(Usuario usuario);
    public void actualizar(Usuario usuario);
    public void eliminar(int id);
    public List<Usuario> listarUsuarios();
    public Usuario usuarioPorId(int id);
    public Usuario usuarioPorUsername(String username);

}
