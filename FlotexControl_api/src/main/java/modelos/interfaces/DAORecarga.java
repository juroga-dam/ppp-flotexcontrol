package modelos.interfaces;

import modelos.dto.Recarga;

import java.util.List;

public interface DAORecarga {
    public void insertar(Recarga recarga);
    public void actualizar(Recarga recarga);
    public void eliminar(int id);
    public List<Recarga> listarRecargas();
    public Recarga recargaPorId(int id);

}
