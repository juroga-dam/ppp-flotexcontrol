package modelos.interfaces;

import modelos.dto.Mantenimiento;

import java.util.List;

public interface DAOMantenimiento {
    public void insertar(Mantenimiento mantenimiento);
    public void actualizar(Mantenimiento mantenimiento);
    public void eliminar(int id);
    public List<Mantenimiento> listarMantenimientos();
    public Mantenimiento mantenimientoPorId(int id);

}
