package modelos.interfaces;

import modelos.dto.Rol;
import java.util.List;

public interface DAORol {
    public void insertar(Rol rol);
    public void actualizar(Rol rol);
    public void eliminar(int id);
    public List<Rol> listarRoles();
}
