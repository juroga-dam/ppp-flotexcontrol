package modelos.interfaces;

import modelos.dto.Repostaje;

import java.util.List;

public interface DAORepostaje {
    public void insertar(Repostaje repostaje);
    public void actualizar(Repostaje repostaje);
    public void eliminar(int id);
    public List<Repostaje> listarRepostajes();
    public Repostaje repostajePorId(int id);

}
