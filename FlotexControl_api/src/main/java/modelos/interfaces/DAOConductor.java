package modelos.interfaces;

import modelos.dto.Conductor;

import java.util.List;

public interface DAOConductor {
    public void insertar(Conductor conductor);
    public void actualizar(Conductor conductor);
    public void eliminar(int id);
    public List<Conductor> listarConductores();
    public Conductor conductorPorId(int id);
    public void asignarVehiculo(int id_conductor, int id_vehiculo);
}
