package modelos.dto;

public class Usuario {
    private int ID_usuario;
    private String nombre;
    private String apellidos;
    private String username;
    private String contrasenia;
    private int rol;
    private Boolean activo;


    public Usuario() {
    }
    
    public Usuario(int ID_usuario, String nombre, String contrasenia, int rol) {
        this.ID_usuario = ID_usuario;
        this.nombre = nombre;
        this.username = username;
        this.contrasenia = contrasenia;
        this.rol = rol;
    }


    public Usuario(int ID_usuario, int rol, Boolean activo) {
        this.ID_usuario = ID_usuario;
        this.rol = rol;
        this.activo = activo;

    }

    public int getID_usuario() {
        return ID_usuario;
    }

    public void setID_usuario(int ID_usuario) {
        this.ID_usuario = ID_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() { return apellidos; }

    public void setApellidos(String apellidos) { this.apellidos = apellidos; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public Boolean getActivo() { return activo; }

    public void setActivo(Boolean activo) { this.activo = activo; }
      
}


