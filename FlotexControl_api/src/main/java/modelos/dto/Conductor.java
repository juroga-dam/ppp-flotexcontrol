package modelos.dto;

public class Conductor {
    private int ID_conductor;
    private String nombre;
    private String apellido;
    private String licencia_conducir;
    private int ID_vehiculo;

    public Conductor() {
    }

    public Conductor(int ID_conductor, String nombre, String apellido, String licencia_conducir, int ID_vehiculo) {
        this.ID_conductor = ID_conductor;
        this.nombre = nombre;
        this.apellido = apellido;
        this.licencia_conducir = licencia_conducir;
        this.ID_vehiculo = ID_vehiculo;
    }

    public Conductor(String nombre, String apellido, String licencia_conducir) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.licencia_conducir = licencia_conducir;
    }

    public int getID_conductor() {
        return ID_conductor;
    }

    public void setID_conductor(int ID_conductor) {
        this.ID_conductor = ID_conductor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getLicencia_conducir() {
        return licencia_conducir;
    }

    public void setLicencia_conducir(String licencia_conducir) {
        this.licencia_conducir = licencia_conducir;
    }

    public int getID_vehiculo() {
        return ID_vehiculo;
    }

    public void setID_vehiculo(int ID_vehiculo) {
        this.ID_vehiculo = ID_vehiculo;
    }
    
    
}
