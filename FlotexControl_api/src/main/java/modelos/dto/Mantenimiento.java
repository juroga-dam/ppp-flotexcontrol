package modelos.dto;

import java.sql.Date;

/**
 *
 * @author crass
 */
public class Mantenimiento {
    private int ID_mantenimiento;
    private String tipo_mantenimiento;
    private Date fecha;
    private String descripcion;
    private int ID_vehiculo;

    public Mantenimiento() {
    }

    public Mantenimiento(int ID_mantenimiento, String tipo_mantenimiento, Date fecha, String descripcion, int ID_vehiculo) {
        this.ID_mantenimiento = ID_mantenimiento;
        this.tipo_mantenimiento = tipo_mantenimiento;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.ID_vehiculo = ID_vehiculo;
    }

    public int getID_mantenimiento() {
        return ID_mantenimiento;
    }

    public void setID_mantenimiento(int ID_mantenimiento) {
        this.ID_mantenimiento = ID_mantenimiento;
    }

    public String getTipo_mantenimiento() {
        return tipo_mantenimiento;
    }

    public void setTipo_mantenimiento(String tipo_mantenimiento) {
        this.tipo_mantenimiento = tipo_mantenimiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getID_vehiculo() {
        return ID_vehiculo;
    }

    public void setID_vehiculo(int ID_vehiculo) {
        this.ID_vehiculo = ID_vehiculo;
    }
    
    
    
}
