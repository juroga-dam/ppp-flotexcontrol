package modelos.dto;

import java.sql.Date;

/**
 *
 * @author crass
 */
public class Recarga {
    private int ID_recarga;
    private Date fecha;
    private double cantidad_kWh;
    private double costo_kWh;
    private int ID_vehiculo;

    public Recarga() {
    }

    public Recarga(int ID_recarga, Date fecha, double cantidad_kWh, double costo_kWh, int ID_vehiculo) {
        this.ID_recarga = ID_recarga;
        this.fecha = fecha;
        this.cantidad_kWh = cantidad_kWh;
        this.costo_kWh = costo_kWh;
        this.ID_vehiculo = ID_vehiculo;
    }

    public int getID_recarga() {
        return ID_recarga;
    }

    public void setID_recarga(int ID_recarga) {
        this.ID_recarga = ID_recarga;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getCantidad_kWh() {
        return cantidad_kWh;
    }

    public void setCantidad_kWh(double cantidad_kWh) {
        this.cantidad_kWh = cantidad_kWh;
    }

    public double getCosto_kWh() {
        return costo_kWh;
    }

    public void setCosto_kWh(double costo_kWh) {
        this.costo_kWh = costo_kWh;
    }

    public int getID_vehiculo() {
        return ID_vehiculo;
    }

    public void setID_vehiculo(int ID_vehiculo) {
        this.ID_vehiculo = ID_vehiculo;
    }
    
    
}
