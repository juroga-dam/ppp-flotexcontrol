package modelos.dto;

import java.util.Date;

/**
 *
 * @author crass
 */
public class Repostaje {
    private int ID_repostaje;
    private Date fecha;
    private double cantidad_litros;
    private double costo_litro;
    private int ID_vehiculo;

    public Repostaje() {
    }

    public Repostaje(int ID_repostaje, Date fecha, double cantidad_litros, double costo_litro, int ID_vehiculo) {
        this.ID_repostaje = ID_repostaje;
        this.fecha = fecha;
        this.cantidad_litros = cantidad_litros;
        this.costo_litro = costo_litro;
        this.ID_vehiculo = ID_vehiculo;
    }

    public int getID_repostaje() {
        return ID_repostaje;
    }

    public void setID_repostaje(int ID_repostaje) {
        this.ID_repostaje = ID_repostaje;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getCantidad_litros() {
        return cantidad_litros;
    }

    public void setCantidad_litros(double cantidad_litros) {
        this.cantidad_litros = cantidad_litros;
    }

    public double getCosto_litro() {
        return costo_litro;
    }

    public void setCosto_litro(double costo_litro) {
        this.costo_litro = costo_litro;
    }

    public int getID_vehiculo() {
        return ID_vehiculo;
    }

    public void setID_vehiculo(int ID_vehiculo) {
        this.ID_vehiculo = ID_vehiculo;
    }
    
    
}
