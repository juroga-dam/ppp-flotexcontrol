package modelos.dto;

import java.util.Date;

public class Alerta {
     private int ID_alerta;
    private String tipo;
    private Date fecha;
    private String descripcion;
    private int ID_usuario;
    private boolean mostrar;

    public Alerta() {
    }

    public Alerta(int ID_alerta, String tipo, Date fecha, String descripcion, int ID_usuario, boolean mostrar) {
        this.ID_alerta = ID_alerta;
        this.tipo = tipo;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.ID_usuario = ID_usuario;
        this.mostrar = mostrar;
    }

    public int getID_alerta() {
        return ID_alerta;
    }

    public void setID_alerta(int ID_alerta) {
        this.ID_alerta = ID_alerta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getID_usuario() {
        return ID_usuario;
    }

    public void setID_usuario(int ID_usuario) {
        this.ID_usuario = ID_usuario;
    }

    public void setMostrar(boolean mostrar) { this.mostrar = mostrar; }

    public boolean getMostrar() { return mostrar; }
        
}
