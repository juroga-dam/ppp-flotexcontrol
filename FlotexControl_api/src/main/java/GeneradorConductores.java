import controladores.ConductorControlador;

public class GeneradorConductores {
    public static void main(String[] args) {
        ConductorControlador controlador = new ConductorControlador();

        for (int i = 1; i <= 15; i++) {
            String nombre = "Nombre" + i;
            String apellido = "Apellido" + i;
            String licencia = "Licencia" + i;

            controlador.crearConductor(nombre, apellido, licencia);
            System.out.println("Conductor " + i + " creado: " + nombre + " " + apellido + ", Licencia: " + licencia);
        }
    }
}
