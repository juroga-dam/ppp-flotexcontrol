import java.util.Random;
import modelos.dao.DAOVehiculoImpl;
import modelos.dto.Vehiculo;


public class GeneradorVehiculos {
    public static void main(String[] args) {
        String[][] modelos = {
                {"Toyota", "Corolla", "Yaris", "Rav4"},
                {"Honda", "Civic", "Accord", "CR-V"},
                {"Ford", "Fiesta", "Focus", "Mustang"},
                {"Chevrolet", "Spark", "Aveo", "Cruze"},
                {"Nissan", "Versa", "Sentra", "X-Trail"},
                {"Volkswagen", "Gol", "Jetta", "Tiguan"},
                {"Hyundai", "Accent", "Elantra", "Tucson"},
                {"Kia", "Rio", "Forte", "Sportage"},
                {"Mazda", "2", "3", "CX-5"},
                {"Renault", "Clio", "Duster", "Kwid"},
                {"Peugeot", "208", "301", "2008"},
                {"Fiat", "Uno", "Mobi", "Toro"},
                {"Suzuki", "Swift", "Ciaz", "Vitara"},
                {"Mitsubishi", "Mirage", "Lancer", "Outlander"},
                {"Audi", "A1", "A3", "Q3"},
                {"BMW", "Serie 1", "Serie 3", "X1"},
                {"Mercedes-Benz", "Clase A", "Clase C", "GLA"},
                {"Volvo", "S40", "S60", "XC40"},
                {"Jaguar", "XE", "XF", "F-Pace"},
                {"Land Rover", "Discovery", "Range Rover", "Evoque"},
                {"Jeep", "Renegade", "Compass", "Wrangler"},
                {"Subaru", "Impreza", "Legacy", "Forester"},
                {"Chrysler", "300", "Pacifica", "Voyager"},
                {"Dodge", "Attitude", "Journey", "Durango"},
                {"Buick", "Encore", "Enclave", "Regal"},
                {"Cadillac", "ATS", "CT4", "XT4"},
                {"GMC", "Terrain", "Acadia", "Yukon"},
                {"Acura", "ILX", "TLX", "RDX"},
                {"Infiniti", "Q50", "Q60", "QX50"},
                {"Lexus", "IS", "ES", "NX"},
                {"Tesla", "Model 3", "Model S", "Model X"},
                {"Porsche", "911", "Panamera", "Cayenne"},
                {"Ferrari", "Portofino", "Roma", "F8 Tributo"},
                {"Lamborghini", "Huracán", "Aventador", "Urus"},
                {"Maserati", "Ghibli", "Quattroporte", "Levante"},
                {"Alfa Romeo", "Giulietta", "Stelvio", "4C"},
                {"Lotus", "Elise", "Evora", "Exige"},
                {"McLaren", "570S", "720S", "GT"},
                {"Bugatti", "Chiron", "Divo", "Centodieci"},
                {"Koenigsegg", "Jesko", "Regera", "Gemera"},
                {"Pagani", "Huayra", "Zonda", "Imola"},
                {"Aston Martin", "Vantage", "DB11", "DBS Superleggera"},
                {"Bentley", "Continental", "Flying Spur", "Bentayga"},
                {"Rolls-Royce", "Phantom", "Ghost", "Cullinan"},
                {"Maybach", "S 560", "S 650", "GLS 600"},
                {"Volvo", "S40", "S60", "XC40"},
                {"Jaguar", "XE", "XF", "F-Pace"},
                {"Land Rover", "Discovery", "Range Rover", "Evoque"}
        };

        Random random = new Random();
        DAOVehiculoImpl dao = new DAOVehiculoImpl();

        for (int i = 0; i < 30; i++) {
            int ID_vehiculo = i + 1;
            int pos = random.nextInt(modelos.length);
            String marca = modelos[pos][0];
            String modelo = modelos[pos][random.nextInt(3) + 1];
            int anio = 2024 - random.nextInt(20);
            String tipoCombustible = generarTipoCombustible();
            int capacidadCarga = random.nextInt(150 - 45) + 45;
            String tipoMotor = "Combustión";

            if (tipoCombustible.equals("Eléctrico")) {
                tipoMotor = "Eléctrico";
            }

            String matricula = generarMatricula();

            Vehiculo vehiculo = new Vehiculo(ID_vehiculo, matricula, marca, modelo, anio, tipoCombustible, capacidadCarga, tipoMotor);
            DAOVehiculoImpl daoVehiculo = new DAOVehiculoImpl();
            daoVehiculo.insertar(vehiculo);
        }
    }

    // Genera un tipo de combustible aleatorio
    private static String generarTipoCombustible() {
        Random random = new Random();
        int probabilidad = random.nextInt(100) + 1;

        if (probabilidad <= 18) {
            return "Eléctrico";
        } else if (probabilidad <= 60) {
            return "Gasolina";
        } else {
            return "Diesel";
        }
    }

    // Genera una matrícula aleatoria válida
    private static String generarMatricula() {
        Random random = new Random();
        String letras = "BCDFGHJKLMNPQRSTVWXYZ"; // Sólo consonantes, no se usan vocales
        StringBuilder matricula = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            matricula.append(random.nextInt(9) + 1); // Números del 1 al 9
        }

        matricula.append(" ");

        for (int i = 0; i < 3; i++) {
            matricula.append(letras.charAt(random.nextInt(letras.length())));
        }

        return matricula.toString();
    }
}