import controladores.VehiculoControlador;
import modelos.dao.DAOVehiculoImpl;
import modelos.dto.Vehiculo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class VehiculoIntegracionTest {

    private DAOVehiculoImpl dao = new DAOVehiculoImpl();
    private VehiculoControlador controlador;
    private Vehiculo vehiculoCreado;

    @BeforeEach
    void setUp() {
        controlador = new VehiculoControlador();
        // Elimina todos los vehiculos de la tabla
        List<Vehiculo> vehiculos = controlador.listarVehiculos();
        for (Vehiculo v : vehiculos) {
            controlador.eliminarVehiculo(v.getID_vehiculo());
        }
        dao.resetIDTable();
    }

    @Test
    void testFlujoCompletoVehiculo() {
        // Crear y verificar inserción
        controlador.crearVehiculo("1234XYZ", "TestMarca", "TestModelo", 2020, "Gasolina", 1500, "Combustión");
        vehiculoCreado = controlador.listarVehiculos().stream().findFirst().orElse(null);
        assertNotNull(vehiculoCreado, "El vehículo debería haber sido creado");

        // Actualizar y verificar actualización

        controlador.actualizarVehiculo( controlador.buscarVehiculoMatricula(vehiculoCreado.getMatricula()).getID_vehiculo(),"9876ABC", "MarcaActualizada", "ModeloActualizado", 2021, "Eléctrico", 1200, "Eléctrico");
        Vehiculo vehiculoActualizado = (controlador.buscarVehiculoId(vehiculoCreado.getID_vehiculo()));
        assertEquals("ModeloActualizado", vehiculoActualizado.getModelo(), "El modelo del vehículo debería haberse actualizado");

        // Eliminar y verificar eliminación
        controlador.eliminarVehiculo(vehiculoActualizado.getID_vehiculo());
        assertNull(controlador.buscarVehiculoId(vehiculoActualizado.getID_vehiculo()), "El vehículo debería haber sido eliminado");
    }

    @AfterEach
    void tearDown() {
        // Elimina todos los vehiculos de la tabla
        List<Vehiculo> vehiculos = controlador.listarVehiculos();
        for (Vehiculo v : vehiculos) {
            controlador.eliminarVehiculo(v.getID_vehiculo());
        }
    }
}
