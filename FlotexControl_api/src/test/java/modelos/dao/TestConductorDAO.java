package modelos.dao;

import controladores.ConductorControlador;
import controladores.VehiculoControlador;
import modelos.dto.Alerta;
import modelos.dto.Conductor;
import modelos.dto.Vehiculo;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestConductorDAO {

    private static DAOConductorImpl daoConductor;

/*
    @BeforeAll
    public static void setUpClass() {

        // Aquí deberías asegurarte de que la conexión a la base de datos de prueba está configurada correctamente.
    }
*/

    @BeforeEach
    public void setUp() {
        daoConductor = new DAOConductorImpl();
        Conductor nuevoConductor = new Conductor("Prueba", "Apellido", "Licencia000");
        daoConductor.insertar(nuevoConductor);
    }

    @Test
    @Order(1)
    public void testInsertar() {

        List<Conductor> conductores = daoConductor.listarConductores();
        boolean encontrado = conductores.stream().anyMatch(c -> c.getNombre().equals("Prueba") && c.getApellido().equals("Apellido"));
        assertTrue(encontrado, "El conductor debe ser insertado en la base de datos.");
    }

    @Test
    @Order(2)
    public void testActualizar() {

        Conductor conductorActualizado = new Conductor(1, "NombreActualizado", "ApellidoActualizado", "LicenciaActualizada", 0);
        daoConductor.actualizar(conductorActualizado);

        Conductor resultado = daoConductor.conductorPorId(1);
        assertEquals("NombreActualizado", resultado.getNombre(), "El nombre del conductor debe ser actualizado.");
    }

    @Test
    @Order(3)
    public void testEliminar() {
        // Asumiendo que el conductor con ID 1 existe
        daoConductor.eliminar(1);
        assertNull(daoConductor.conductorPorId(1), "El conductor debe ser eliminado de la base de datos.");
    }

    @Test
    @Order(4)
    public void testListarConductores() {

        List<Conductor> conductores = daoConductor.listarConductores();
        assertFalse(conductores.isEmpty(), "Debe haber al menos un conductor en la base de datos.");
    }

    @Test
    @Order(5)
    public void testConductorPorId() {
        Conductor conductor = daoConductor.conductorPorId(1);
        assertNotNull(conductor, "Debe poder recuperar un conductor por su ID.");
    }

    @Test
    @Order(6)
    public void testAsignarVehiculo() {

        int idVehiculo = obtenerIDVehiculo();
        daoConductor.asignarVehiculo(1, idVehiculo);
        Conductor conductorBuscado = daoConductor.conductorPorId(1);
        int idVehiculo_conductor = conductorBuscado.getID_vehiculo();
        System.out.println(conductorBuscado.getID_vehiculo());
        assertEquals(idVehiculo, idVehiculo_conductor, "El ID del vehículo debe ser asignado correctamente al conductor.");
    }

    @AfterEach
    public void tearDown() {
        // Elimina todos los conductores de la tabla
        List<Conductor> conductores = daoConductor.listarConductores();
        for (Conductor c : conductores) {
            daoConductor.eliminar(c.getID_conductor());
        }
        daoConductor.resetIDTable();
    }

    private int obtenerIDVehiculo() {
        VehiculoControlador controlador = new VehiculoControlador();
        List<Vehiculo> listaVehiculos = controlador.listarVehiculos();
        assertFalse(listaVehiculos.isEmpty(), "La lista de vehiculos no debe estar vacía");
        Vehiculo vehiculoExistente = listaVehiculos.get(0);
        int idVehiculoExistente = vehiculoExistente.getID_vehiculo();
        return idVehiculoExistente;
    }
}