package modelos.dao;

import modelos.dao.DAOVehiculoImpl;
import modelos.dto.Vehiculo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestVehiculoDAO {

    private DAOVehiculoImpl vehiculoDAO;
    private Vehiculo vehiculoTest, vehiculoBuscado;

    @BeforeEach
    void setUp() {

        vehiculoDAO = new DAOVehiculoImpl();

        vehiculoTest = new Vehiculo();
        vehiculoTest.setMatricula("TEST123");
        vehiculoTest.setMarca("TestMarca");
        vehiculoTest.setModelo("TestModelo");
        vehiculoTest.setAnio(2020);
        vehiculoTest.setTipoCombustible("Gasolina");
        vehiculoTest.setCapacidadCarga(1000);
        vehiculoTest.setTipoMotor("TestTipoMotor");


    }

    @Test
    void testInsertarYRecuperarVehiculo() {
        // Insertar el vehículo de prueba en la base de datos
        vehiculoDAO.insertar(vehiculoTest);
        // Intentar recuperar el vehículo de prueba por su matrícula
        Vehiculo vehiculoRecuperado = vehiculoDAO.vehiculoPorMatricula(vehiculoTest.getMatricula());
        assertNotNull(vehiculoRecuperado, "El vehículo recuperado no debería ser nulo");
        assertEquals(vehiculoTest.getMatricula(), vehiculoRecuperado.getMatricula(), "Las matrículas deberían coincidir");

    }

    @Test
    void testActualizarVehiculo() {
        vehiculoDAO.insertar(vehiculoTest);
        vehiculoTest = vehiculoDAO.vehiculoPorMatricula(vehiculoTest.getMatricula());

        // Actualiza datos
        vehiculoTest.setMarca("MarcaActualizada");
        vehiculoTest.setModelo("ModeloActualizado");
        vehiculoTest.setAnio(2021);

        vehiculoDAO.actualizar(vehiculoTest);

        // Recuperar el vehículo actualizado para verificar cambios
        Vehiculo vehiculoActualizado = vehiculoDAO.vehiculoPorMatricula(vehiculoTest.getMatricula());

        assertNotNull(vehiculoActualizado, "El vehículo actualizado no debería ser nulo");
        assertEquals("MarcaActualizada", vehiculoActualizado.getMarca(), "La marca debería haber sido actualizada");
        assertEquals("ModeloActualizado", vehiculoActualizado.getModelo(), "El modelo debería haber sido actualizado");
        assertEquals(2021, vehiculoActualizado.getAnio(), "El año debería haber sido actualizado");
    }

    @Test
    void testListarVehiculos() {
        vehiculoDAO.insertar(vehiculoTest);

        // Recuperar la lista de vehículos
        List<Vehiculo> vehiculos = vehiculoDAO.listarVehiculos();

        assertFalse(vehiculos.isEmpty(), "La lista de vehículos no debería estar vacía");
        assertTrue(vehiculos.stream().anyMatch(v -> v.getMatricula().equals(vehiculoTest.getMatricula())), "La lista debe contener el vehículo de prueba");
    }

    @AfterEach
    void tearDown() {
        // Limpiar la base de datos de prueba
        vehiculoBuscado = new Vehiculo();
        vehiculoBuscado = vehiculoDAO.vehiculoPorMatricula(vehiculoTest.getMatricula());
        vehiculoDAO.eliminar(vehiculoBuscado.getID_vehiculo());
    }
}