package modelos.dao;

import modelos.dto.Alerta;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.sql.Date;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class TestAlertaDAO {

    private DAOAlertaImpl daoAlertaImpl;

    @BeforeEach
    void setUp() {
        daoAlertaImpl = new DAOAlertaImpl();
    }

    @Test
    void insertarYListarAlertasTest() {
        Alerta alerta = new Alerta();
        alerta.setTipo("Prueba");
        alerta.setFecha(new Date(System.currentTimeMillis()));
        alerta.setDescripcion("Descripción de prueba");
        alerta.setID_usuario(1);
        alerta.setMostrar(true);

        daoAlertaImpl.insertar(alerta);

        List<Alerta> listaAlertas = daoAlertaImpl.listarAlertas();
        assertFalse(listaAlertas.isEmpty(), "La lista de alertas no debe estar vacía después de insertar");

        // Comprueba que la alerta insertada esté en la base de datos.
        assertTrue(listaAlertas.stream().anyMatch(a ->
                        a.getTipo().equals("Prueba") &&
                                a.getDescripcion().equals("Descripción de prueba") &&
                                a.getID_usuario() == 1 &&
                                a.getMostrar()),
                "Debe existir la alerta insertada en la lista de alertas");
    }

    @Test
    void actualizarYAlertaPorIdTest() {
        List<Alerta> listaAlertas = daoAlertaImpl.listarAlertas();
        assertFalse(listaAlertas.isEmpty(), "La lista de alertas no debe estar vacía antes de eliminar");
        Alerta alertaExistente = listaAlertas.get(0);
        int idAlertaExistente = alertaExistente.getID_alerta();
        Alerta alerta = new Alerta();
        alerta.setID_alerta(idAlertaExistente);
        alerta.setTipo("Actualizado");
        alerta.setFecha(new Date(System.currentTimeMillis()));
        alerta.setDescripcion("Descripción actualizada");
        alerta.setID_usuario(1);
        alerta.setMostrar(false);

        daoAlertaImpl.actualizar(alerta);

        Alerta alertaActualizada = daoAlertaImpl.alertaPorId(idAlertaExistente);
        assertNotNull(alertaActualizada, "La alerta actualizada no debe ser nula");
        assertEquals("Actualizado", alertaActualizada.getTipo(), "El tipo de alerta debe haberse actualizado");
        assertEquals("Descripción actualizada", alertaActualizada.getDescripcion(), "La descripción de la alerta debe haberse actualizado");
        assertFalse(alertaActualizada.getMostrar(), "El estado mostrar de la alerta debe haberse actualizado a false");
    }

    @Test
    void listarAlertasTest() {
        List<Alerta> listaAlertas = daoAlertaImpl.listarAlertas();
        assertNotNull(listaAlertas, "La lista de alertas no debe ser nula");
        assertTrue(listaAlertas.size() >= 0, "La lista de alertas debe ser capaz de estar vacía o contener alertas");
    }


    @Test
    void alertaPorIdTest() {
        List<Alerta> listaAlertas = daoAlertaImpl.listarAlertas();

        assertNotNull(listaAlertas, "La lista de alertas no debe estar vacía");
        assertFalse(listaAlertas.isEmpty(), "La lista de alertas no debe estar vacía");

        // Selecciona una alerta al azar de la lista
        Random rand = new Random();
        int indiceAleatorio = rand.nextInt(listaAlertas.size());
        Alerta alertaAleatoria = listaAlertas.get(indiceAleatorio);
        int idAlertaAleatoria = alertaAleatoria.getID_alerta();

        Alerta alerta = daoAlertaImpl.alertaPorId(idAlertaAleatoria);
        assertNotNull(alerta, "La alerta con el ID especificado debe existir");
        assertEquals(idAlertaAleatoria, alerta.getID_alerta(), "El ID de la alerta seleccionada al azar debe coincidir con el solicitado");
    }

    @Test
    void eliminarAlertaTest() {
        List<Alerta> listaAlertas = daoAlertaImpl.listarAlertas();
        assertFalse(listaAlertas.isEmpty(), "La lista de alertas no debe estar vacía antes de eliminar");

        // Seleccionamos la primera alerta para eliminar
        Alerta alertaAEliminar = listaAlertas.get(0);
        daoAlertaImpl.eliminar(alertaAEliminar.getID_alerta());

        // Verificamos que la alerta ya no esté en la lista
        Alerta alertaEliminada = daoAlertaImpl.alertaPorId(alertaAEliminar.getID_alerta());
        assertNotNull(alertaEliminada, "La alerta eliminada debe existir en la base de datos");
        assertFalse(alertaEliminada.getMostrar(), "La alerta eliminada debe tener el campo mostrar en false");
    }

}
