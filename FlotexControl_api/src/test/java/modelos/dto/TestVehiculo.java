package modelos.dto;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

    class TestVehiculo {

        @Test
        void testCrearYObtenerPropiedadesVehiculo() {
            int ID_vehiculo = 1;
            String matricula = "ABC123";
            String marca = "Toyota";
            String modelo = "Corolla";
            int anio = 2020;
            String tipoCombustible = "Gasolina";
            int capacidadCarga = 150;
            String tipoMotor = "1.8L";

            Vehiculo vehiculo = new Vehiculo(ID_vehiculo, matricula, marca, modelo, anio, tipoCombustible, capacidadCarga, tipoMotor);

            // getters
            assertEquals(ID_vehiculo, vehiculo.getID_vehiculo(), "El ID del vehículo no coincide");
            assertEquals(matricula, vehiculo.getMatricula(), "La matrícula del vehículo no coincide");
            assertEquals(marca, vehiculo.getMarca(), "La marca del vehículo no coincide");
            assertEquals(modelo, vehiculo.getModelo(), "El modelo del vehículo no coincide");
            assertEquals(anio, vehiculo.getAnio(), "El año del vehículo no coincide");
            assertEquals(tipoCombustible, vehiculo.getTipoCombustible(), "El tipo de combustible del vehículo no coincide");
            assertEquals(capacidadCarga, vehiculo.getCapacidadCarga(), "La capacidad de carga del vehículo no coincide");
            assertEquals(tipoMotor, vehiculo.getTipoMotor(), "El tipo de motor del vehículo no coincide");

            // Probar setters con nuevos valores
            vehiculo.setID_vehiculo(2);
            vehiculo.setMatricula("XYZ789");
            vehiculo.setMarca("Honda");
            vehiculo.setModelo("Civic");
            vehiculo.setAnio(2021);
            vehiculo.setTipoCombustible("Eléctrico");
            vehiculo.setCapacidadCarga(120);
            vehiculo.setTipoMotor("2.0L");

            // Verificar que hayan actualizado los valores
            assertEquals(2, vehiculo.getID_vehiculo(), "El ID del vehículo actualizado no coincide");
            assertEquals("XYZ789", vehiculo.getMatricula(), "La matrícula del vehículo actualizada no coincide");
            assertEquals("Honda", vehiculo.getMarca(), "La marca del vehículo actualizada no coincide");
            assertEquals("Civic", vehiculo.getModelo(), "El modelo del vehículo actualizado no coincide");
            assertEquals(2021, vehiculo.getAnio(), "El año del vehículo actualizado no coincide");
            assertEquals("Eléctrico", vehiculo.getTipoCombustible(), "El tipo de combustible del vehículo actualizado no coincide");
            assertEquals(120, vehiculo.getCapacidadCarga(), "La capacidad de carga del vehículo actualizada no coincide");
            assertEquals("2.0L", vehiculo.getTipoMotor(), "El tipo de motor del vehículo actualizado no coincide");
        }
    }
