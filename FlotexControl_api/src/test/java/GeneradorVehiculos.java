import java.util.Random;
import modelos.dao.DAOVehiculoImpl;
import modelos.dto.Vehiculo;


public class GeneradorVehiculos {
    public static void main(String[] args) {
        String[][] modelos = {
                {"Toyota", "Corolla", "Yaris", "Rav4"},
                {"Honda", "Civic", "Accord", "CR-V"},
                {"Ford", "Fiesta", "Focus", "Mustang"},
                // Añade más marcas y modelos según sea necesario
        };

        Random random = new Random();
        DAOVehiculoImpl dao = new DAOVehiculoImpl();

        for (int i = 0; i < 30; i++) {
            int ID_vehiculo = i + 1;
            int pos = random.nextInt(modelos.length);
            String marca = modelos[pos][0];
            String modelo = modelos[pos][random.nextInt(3) + 1];
            int anio = 2022 - random.nextInt(20); // Asumiendo que el año de fabricación está entre 2002 y 2022
            String tipoCombustible = generarTipoCombustible();
            int capacidadCarga = random.nextInt(150 - 45) + 45;
            String tipoMotor = "Combustión"; // Por defecto, la mayoría será Diesel

            if (tipoCombustible.equals("Eléctrico")) {
                tipoMotor = "Eléctrico";
            }

            String matricula = generarMatricula();

            Vehiculo vehiculo = new Vehiculo(ID_vehiculo, matricula, marca, modelo, anio, tipoCombustible, capacidadCarga, tipoMotor);
            DAOVehiculoImpl daoVehiculo = new DAOVehiculoImpl();
            daoVehiculo.insertar(vehiculo);
        }
    }

    // Genera un tipo de combustible según la probabilidad dada
    private static String generarTipoCombustible() {
        Random random = new Random();
        int probabilidad = random.nextInt(100) + 1;

        if (probabilidad <= 18) {
            return "Eléctrico";
        } else if (probabilidad <= 60) {
            return "Gasolina";
        } else {
            return "Diesel";
        }
    }

    // Genera una matrícula aleatoria válida
    private static String generarMatricula() {
        Random random = new Random();
        String letras = "BCDFGHJKLMNPQRSTVWXYZ"; // Sólo consonantes, no se usan vocales
        StringBuilder matricula = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            matricula.append(random.nextInt(9) + 1); // Números del 1 al 9
        }

        matricula.append(" ");

        for (int i = 0; i < 3; i++) {
            matricula.append(letras.charAt(random.nextInt(letras.length())));
        }

        return matricula.toString();
    }
}