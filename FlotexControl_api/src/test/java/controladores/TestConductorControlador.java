package controladores;

import controladores.ConductorControlador;
import modelos.dao.DAOConductorImpl;
import modelos.dto.Conductor;
import modelos.dto.Vehiculo;
import org.junit.jupiter.api.*;

        import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestConductorControlador {

    private controladores.ConductorControlador controlador = new ConductorControlador();
    private DAOConductorImpl daoConductor = new DAOConductorImpl();

/*    @BeforeAll
    public static void setUpClass() {
        controlador = new ConductorControlador();
        // Aquí deberías asegurarte de que la conexión a la base de datos de prueba está configurada correctamente.
    }*/

    @BeforeEach
    public void setUp() {
        controlador.crearConductor("Test", "Conductor", "LicenciaTest");
    }

    @Test
    @Order(1)
    public void testCrearConductor() {
        controlador.crearConductor("Test", "Conductor", "LicenciaTest");
        List<Conductor> conductores = controlador.listarConductores();
        assertTrue(conductores.stream().anyMatch(c -> c.getNombre().equals("Test") && c.getApellido().equals("Conductor")), "El conductor debe ser creado.");
    }

    @Test
    @Order(2)
    public void testActualizarConductor() {
        List<Conductor> conductores = controlador.listarConductores();
        Conductor conductor = conductores.get(0);
        controlador.actualizarConductor(conductor.getID_conductor(), "NombreActualizado", "ApellidoActualizado", "LicenciaActualizada");
        Conductor actualizado = controlador.buscarConductor(conductor.getID_conductor());
        assertEquals("NombreActualizado", actualizado.getNombre(), "El nombre del conductor debe ser actualizado.");
    }

    @Test
    @Order(3)
    public void testEliminarConductor() {
        List<Conductor> conductores = controlador.listarConductores();
        Conductor conductor = conductores.get(0);
        controlador.eliminarConductor(conductor.getID_conductor());
        Conductor resultado = controlador.buscarConductor(conductor.getID_conductor());
        assertNull(resultado, "El conductor debe ser eliminado.");
    }

    @Test
    @Order(4)
    public void testListarConductores() {
        List<Conductor> conductores = controlador.listarConductores();
        assertNotNull(conductores, "La lista de conductores no debe ser null.");
    }

    @Test
    @Order(5)
    public void testBuscarConductor() {
        List<Conductor> conductores = controlador.listarConductores();
        Conductor conductor = conductores.get(0);
        Conductor encontrado = controlador.buscarConductor(conductor.getID_conductor());
        assertNotNull(encontrado, "Debe poder recuperar un conductor por su ID.");
    }

    @Test
    @Order(6)
    public void testAsignarVehiculo() {
        List<Conductor> conductores = controlador.listarConductores();
        Conductor conductor = conductores.get(0);
        int idVehiculo = obtenerIDVehiculo(); // Obtener el ID de un vehículo existente
        controlador.asignarVehiculo(conductor.getID_conductor(), idVehiculo);
        Conductor resultado = controlador.buscarConductor(conductor.getID_conductor());
        assertEquals(idVehiculo, resultado.getID_vehiculo(), "El vehículo debe ser asignado correctamente al conductor.");
    }

    @AfterEach
    public void tearDown() {
        // Elimina todos los conductores de la tabla
        List<Conductor> conductores = daoConductor.listarConductores();
        for (Conductor c : conductores) {
            daoConductor.eliminar(c.getID_conductor());
        }
        daoConductor.resetIDTable();
    }

   /* @AfterAll
    public static void tearDownClass() {
        // Cerrar conexión a la base de datos de prueba si es necesario.
    }*/

    private int obtenerIDVehiculo() {
        VehiculoControlador controlador = new VehiculoControlador();
        List<Vehiculo> listaVehiculos = controlador.listarVehiculos();
        assertFalse(listaVehiculos.isEmpty(), "La lista de vehiculos no debe estar vacía");
        Vehiculo vehiculoExistente = listaVehiculos.get(0);
        int idVehiculoExistente = vehiculoExistente.getID_vehiculo();
        return idVehiculoExistente;
    }
}

