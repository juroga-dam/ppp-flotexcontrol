package controladores;

import modelos.dto.Alerta;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class TestAlertaControlador {
    private AlertaControlador controlador;

    @BeforeEach
    void setUp() {
        // Inicializa tu AlertaControlador y cualquier otro setup necesario
        controlador = new AlertaControlador();
    }

    @Test
    void crearYBuscarAlertaTest() {
        // Crear una alerta
        String tipo = "Test";
        Date fecha = new Date();
        String descripcion = "Descripcion de prueba";
        int ID_usuario = 1;
        controlador.crearAlerta(tipo, fecha, descripcion, ID_usuario);

        // Listar las alertas para encontrar una con nuestra descripción
        List<Alerta> alertas = controlador.listarAlertas();
        assertTrue(alertas.stream().anyMatch(a -> a.getDescripcion().equals(descripcion)),
                "Debe existir la alerta que acabamos de crear");

        // Opcional: Buscar una alerta específica por ID si conoces el ID
        // Esto puede ser más complicado sin una forma de obtener el ID de la alerta recién creada
    }

    @Test
    void actualizarAlertaTest() {
        List<Alerta> listaAlertas = controlador.listarAlertas();
        assertFalse(listaAlertas.isEmpty(), "La lista de alertas no debe estar vacía antes de eliminar");
        Alerta alertaExistente = listaAlertas.get(0);
        int idAlertaExistente = alertaExistente.getID_alerta();
        String nuevoTipo = "Tipo actualizado";
        controlador.actualizarAlerta(idAlertaExistente, nuevoTipo, new Date(), "Descripción actualizada", 2);

        Alerta alertaActualizada = controlador.buscarAlerta(idAlertaExistente);
        assertEquals(nuevoTipo, alertaActualizada.getTipo(), "El tipo de la alerta debe haberse actualizado");
    }

    @Test
    void eliminarAlertaTest() {
        List<Alerta> listaAlertas = controlador.listarAlertas();
        assertFalse(listaAlertas.isEmpty(), "La lista de alertas no debe estar vacía antes de eliminar");
        Alerta alertaExistente = listaAlertas.get(0);
        int idAlertaExistente = alertaExistente.getID_alerta();
        controlador.eliminarAlerta(idAlertaExistente);
        Alerta alertaEliminada = controlador.buscarAlerta(idAlertaExistente);
        assertNotNull(alertaEliminada, "La alerta eliminada debe existir en la base de datos");
        assertFalse(alertaEliminada.getMostrar(), "La alerta eliminada debe tener el campo mostrar en false");

        // Opcional: Verificar que la alerta no aparezca en la lista de alertas
    }

    @Test
    void listarAlertasTest() {
        List<Alerta> alertas = controlador.listarAlertas();
        assertNotNull(alertas, "La lista de alertas no debe ser nula");
    }
}
