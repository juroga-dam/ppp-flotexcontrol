package controladores;

import modelos.dto.Vehiculo;
import modelos.dao.DAOVehiculoImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestVehiculoControlador {

    private DAOVehiculoImpl dao = new DAOVehiculoImpl();
    private VehiculoControlador controlador;
    private Vehiculo vehiculoTest;

    @BeforeEach
    void setUp() {
        controlador = new VehiculoControlador();
        // Elimina todos los vehiculos de la tabla
        List<Vehiculo> vehiculos = controlador.listarVehiculos();
        for (Vehiculo v : vehiculos) {
            controlador.eliminarVehiculo(v.getID_vehiculo());
        }
        dao.resetIDTable();
        vehiculoTest = new Vehiculo();
        vehiculoTest.setMatricula("2024TEST");
        vehiculoTest.setMarca("MarcaTest");
        vehiculoTest.setModelo("ModeloTest");
        vehiculoTest.setAnio(2020);
        vehiculoTest.setTipoCombustible("Gasolina");
        vehiculoTest.setCapacidadCarga(1000);
        vehiculoTest.setTipoMotor("Combustión");
        // Crear un vehículo en la base de datos
        controlador.crearVehiculo(vehiculoTest.getMatricula(), vehiculoTest.getMarca(), vehiculoTest.getModelo(), vehiculoTest.getAnio(), vehiculoTest.getTipoCombustible(), vehiculoTest.getCapacidadCarga(), vehiculoTest.getTipoMotor());
    }

    @Test
    void testCrearVehiculo() {
        assertDoesNotThrow(() -> controlador.crearVehiculo("2025TEST","MarcaNuevo","ModeloNuevo", 2021, "Eléctrico", 1200, "Eléctrico"), "Crear vehículo no debería lanzar excepción");
        // Verificar que el vehículo fue creado correctamente
        Vehiculo vehiculoCreado = controlador.listarVehiculos().stream().filter(v -> "ModeloNuevo".equals(v.getModelo()) && v.getAnio() == 2021).findFirst().orElse(null);
        assertNotNull(vehiculoCreado, "El vehículo creado debería existir en la base de datos");
    }
    @Test
    void testActualizarVehiculo() {
        // El vehículoTest ya ha sido agregado a la base de datos en setUp()
        Vehiculo vehiculoParaActualizar = controlador.listarVehiculos().stream()
                .filter(v -> v.getModelo().equals(vehiculoTest.getModelo()))
                .findFirst()
                .orElse(null);
        assertNotNull(vehiculoParaActualizar, "Debe existir el vehículo para actualizar");

        controlador.actualizarVehiculo(vehiculoParaActualizar.getID_vehiculo(), "2026TEST", "MarcaActualizada", "ModeloActualizado", 2021, "Híbrido", 1100, "Híbrido");
        Vehiculo vehiculoActualizado = controlador.buscarVehiculoId(vehiculoParaActualizar.getID_vehiculo());

        assertNotNull(vehiculoActualizado, "El vehículo actualizado no debería ser nulo");
        assertEquals("ModeloActualizado", vehiculoActualizado.getModelo(), "El modelo debería haberse actualizado");
        assertEquals(2021, vehiculoActualizado.getAnio(), "El año debería haberse actualizado");
    }

    @Test
    void testEliminarVehiculo() {
        // Agregar y elimina para la prueba
        Vehiculo vehiculoParaEliminar = new Vehiculo();
        vehiculoParaEliminar.setMatricula("2000AAA");
        vehiculoParaEliminar.setMarca("MarcaParaEliminar");
        vehiculoParaEliminar.setModelo("ModeloParaEliminar");
        vehiculoParaEliminar.setAnio(2022);
        vehiculoParaEliminar.setTipoCombustible("Diesel");
        vehiculoParaEliminar.setCapacidadCarga(1500);
        vehiculoParaEliminar.setTipoMotor("Combustión");
        controlador.crearVehiculo(vehiculoParaEliminar.getMatricula(), vehiculoParaEliminar.getMarca(),vehiculoParaEliminar.getModelo(), vehiculoParaEliminar.getAnio(), vehiculoParaEliminar.getTipoCombustible(), vehiculoParaEliminar.getCapacidadCarga(), vehiculoParaEliminar.getTipoMotor());
        Vehiculo vehiculoRecienCreado = controlador.listarVehiculos().stream()
                .filter(v -> "ModeloParaEliminar".equals(v.getModelo()) && v.getAnio() == 2022)
                .findFirst()
                .orElse(null);
        assertNotNull(vehiculoRecienCreado, "El vehículo para eliminar debería haberse creado correctamente.");

        assertDoesNotThrow(() -> controlador.eliminarVehiculo(vehiculoRecienCreado.getID_vehiculo()),"Eliminar vehículo no debería lanzar excepción");

        Vehiculo vehiculoEliminado = controlador.buscarVehiculoId(vehiculoRecienCreado.getID_vehiculo());
        assertNull(vehiculoEliminado, "El vehículo debería haber sido eliminado correctamente.");
    }

    @Test
    void testListarVehiculos() {
        List<Vehiculo> vehiculos = controlador.listarVehiculos();
        assertFalse(vehiculos.isEmpty(), "La lista de vehículos no debería estar vacía.");
    }

    @Test
    void testBuscarVehiculo() {
        // Se buscará el vehiculo creado en el setUp()
        List<Vehiculo> vehiculos = controlador.listarVehiculos();
        assertFalse(vehiculos.isEmpty(), "Debe existir al menos un vehículo para realizar la prueba");

        Vehiculo vehiculoBuscado = controlador.buscarVehiculoMatricula(vehiculoTest.getMatricula());
        Vehiculo vehiculoEncontrado = controlador.buscarVehiculoId(vehiculoBuscado.getID_vehiculo());

        assertNotNull(vehiculoEncontrado, "Deberíamos encontrar un vehículo con el ID especificado");
        assertEquals(vehiculoBuscado.getID_vehiculo(), vehiculoEncontrado.getID_vehiculo(), "Los IDs del vehículo buscado y encontrado deberían coincidir");
    }

    @AfterEach
    void tearDown() {
        // Elimina todos los vehiculos de la tabla
        List<Vehiculo> vehiculos = controlador.listarVehiculos();
        for (Vehiculo v : vehiculos) {
            controlador.eliminarVehiculo(v.getID_vehiculo());
        }
        dao.resetIDTable();
    }
}